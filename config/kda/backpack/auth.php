<?php

return [
    'allow_force_password_edit'=>true,
    'show_warning'=>true,
    'throw_if_default_route' => true,
    //setup the roles you want to seed
    'seeder_roles' => [
        'webmaster' => [
            'page.read',
            'page.write',
        ],
        'user_manager' => [
            'user.read',
            'user.write',
            'role.read',
            'role.write'
        ],
        'admin' => [
            'admin',
        ],
        'developer' => [
            'permission.read',
            'permission.write',

        ]
    ],
    'seeder_permissions'=>[
        
    ],
    //same for user,with their role
    'seeder_users'=> [
        'admin@example.com'=>[
            'admin'
        ],
        
        'webmaster@example.com'=>[
            'webmaster'
        ]
    ],
];
