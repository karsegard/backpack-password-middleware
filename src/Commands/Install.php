<?php

namespace KDA\Backpack\Auth\Commands;

use GKA\Noctis\Providers\AuthProvider;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Config;

class Install extends Command
{
   
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:backpack-auth:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->call('vendor:publish', ['--provider' => "Spatie\Permission\PermissionServiceProvider",'--tag'=>'migrations']);
        $this->call('vendor:publish', ['--provider' => "Spatie\Permission\PermissionServiceProvider",'--tag'=>'config']);
        $this->call('vendor:publish', ['--provider' => "Backpack\PermissionManager\PermissionManagerServiceProvider", "--tag"=>"config"]);
        $this->call('vendor:publish', ['--provider' => "Backpack\PermissionManager\PermissionManagerServiceProvider", "--tag"=>"lang"]);
        $this->call('vendor:publish', ['--provider' => "Backpack\PermissionManager\PermissionManagerServiceProvider", "--tag"=>"migrations"]);
        $this->call('vendor:publish', ['--provider' => "KDA\Backpack\Auth\ServiceProvider", "--tag"=>"migrations"]);
        $this->call('vendor:publish', ['--provider' => "KDA\Backpack\Auth\ServiceProvider", "--tag"=>"config"]);
        $this->call('vendor:publish', ['--provider' => "KDA\Backpack\Auth\ServiceProvider", "--tag"=>"models","--force"=>true]);
        $this->call('migrate');
        $this->call('db:seed', ['--class' => "\KDA\Backpack\Auth\Seeders\AdminUserSeeder"]);

    }
}
