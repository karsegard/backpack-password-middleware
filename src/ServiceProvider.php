<?php

namespace KDA\Backpack\Auth;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use KDA\Laravel\PackageServiceProvider;


class ServiceProvider extends PackageServiceProvider
{


    use \KDA\Laravel\Traits\HasRoutes;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasMigration;
    use \KDA\Laravel\Traits\HasCommands;

    protected $routes = [
        'backpack/auth.php'
    ];

    protected $configs = [
        'kda/backpack/auth.php'  => 'kda.backpack.auth'
    ];

    protected $registerViews = 'kda/backpack/auth';

    protected $_commands = [
        Commands\Install::class,
        
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishable = [
            'models' => [
                "{$this->path("stubs","Models","User.php.stub")}" => base_path("app/Models/User.php")
            ],
            'seeders' => [
                "{$this->path("database","seeders")}" => base_path("database/seeders")
            ],
        ];
        parent::boot();

        if (config('backpack.base.setup_auth_routes') === true && config('kda.backpack.auth.throw_if_default_route') === true) {
            throw new \Error("Backpack is using the default auth route. Set backpack.base.setup_auth_routes to false or  kda.backpack.auth.throw_if_default_route to false to dismiss");
        }
    }


    /*
    private function registerPublishableResources()
    {
        $configPath = dirname(__DIR__, 1) . '/config/kda/backpack';
     
        $migrationPath =  dirname(__DIR__, 1) . '/database/migrations';
        $seedersPath =  dirname(__DIR__, 1) . '/database/seeders';
        $modelPath =  dirname(__DIR__, 1) . '/stubs/Models';
        $controllerStubs =  dirname(__DIR__, 1) . '/stubs/Controllers';
        $adminControllers = base_path('app/Http/Controllers/Admin');
        //dd($this->getMigrationFileName('password_change_users.php'));
        $publishable = [

            'config' => [
                "{$configPath}/" => config_path('kda/backpack')
            ],

            'routes' => [
                __DIR__ . $this->routeFile => base_path($this->routeFile)
            ],
            'models' => [
                "{$modelPath}/User.php.stub" => base_path("app/Models/User.php")
            ],
            'seeders' => [
                "{$seedersPath}/AdminUserSeeder.php" => base_path("database/seeders/AdminUserSeeder.php")
            ],
            'migrations'=> [
                "{$migrationPath}/password_change_users.php" => $this->getMigrationFileName('password_change_users.php'),
                "{$migrationPath}/disable_users.php" => $this->getMigrationFileName('disable_users.php')

            ],
            'controllers'=> [
                "{$controllerStubs}/PermissionCrudController.php.stub" => $adminControllers."/PermissionCrudController.php",
                "{$controllerStubs}/RoleCrudController.php.stub" => $adminControllers."/RoleCrudController.php",
                "{$controllerStubs}/UserCrudController.php.stub" => $adminControllers."/UserCrudController.php",
            ]

        ];

        foreach ($publishable as $group => $paths) {
            $this->publishes($paths, $group);
        }
    }*/
}
