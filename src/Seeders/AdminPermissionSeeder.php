<?php

namespace KDA\Backpack\Auth\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $guard = 'backpack';
        $roles = config('kda.backpack.auth.seeder_roles');
        
        collect($roles)->reduceWithKeys(function ($carry, $permissions, $role_name) use ($guard) {
            $role = Role::firstOrCreate(['name' => $role_name, 'guard_name' => $guard]);
            dump("creating role {$guard}.{$role_name} ");


            $permissions_subset = collect($permissions)->diff($carry);
            $permissions_subset->map(function ($p) use ($guard, $role) {
                dump("  creating permission {$guard}.{$p} ");
                $permission = Permission::firstOrCreate(['name' => $p, 'guard_name' => $guard]);
            });

            collect($permissions)->map(function ($p) use ($guard, $role) {
                dump("  gave permission {$guard}.{$p} to {$role->name} ");

                $role->givePermissionTo($p);
            });


            return $carry->merge($permissions)->unique();
        }, collect([]));


      

        $permissions_to_seed  = config ('kda.backpack.auth.seeder_permissions');
        collect($permissions_to_seed)->map(function ($p) use ($guard) {
            dump("  creating permission {$guard}.{$p} ");
            $permission = Permission::firstOrCreate(['name' => $p, 'guard_name' => $guard]);
        });
    }
}
