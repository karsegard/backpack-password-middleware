<?php

namespace KDA\Backpack\Auth\Seeders;

use Illuminate\Database\Seeder as LaravelSeeder;

class Seeder extends LaravelSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call([
        PermissionSeeder::class,
        RoleSeeder::class,
        UserSeeder::class,
        RoleHasPermissionSeeder::class,
        ModelHasPermissionSeeder::class,
        ModelHasRoleSeeder::class,


      ]);
    }
}
