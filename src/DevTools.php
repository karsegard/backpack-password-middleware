<?php

namespace  KDA\Backpack\Auth;


class DevTools
{

    public static function seeds(): array
    {
        return [
            'users',
            'roles',
            'permissions',
            'role_has_permissions',
            'model_has_roles',
            'model_has_permissions',

        ];
    }

    public static function sidebars(): array
    {

        return [
            [
                'label' => 'Authentification',
                'icon' => 'la-users-cog',
                'route' => '',
                'from_children' => '1',
                'behavior' => 'disable',
                'lft' => '34',
                'rgt' => '41',
                'depth' => '1',
                'children' => [
                    [
                        'label' => 'Utilisateurs',
                        'icon' => 'la-users',
                        'route' => 'user',
                        'from_children' => '0',
                        'behavior' => 'hide',
                        'lft' => '35',
                        'rgt' => '36',
                        'depth' => '2',
                        'children' => []

                    ],
                    [
                        'label' => 'Rôles',
                        'icon' => 'la-id-badge',
                        'route' => 'role',
                        'from_children' => '0',
                        'behavior' => 'hide',
                        'lft' => '37',
                        'rgt' => '34',
                        'depth' => '2',
                        'children' => []

                    ],
                    [
                        'label' => 'Permissions',
                        'icon' => 'la-key',
                        'route' => 'permission',
                        'from_children' => '0',
                        'behavior' => 'hide',
                        'lft' => '39',
                        'rgt' => '40',
                        'depth' => '2',
                        'children' => []

                    ]
                ]
            ],

        ];
    }
}
