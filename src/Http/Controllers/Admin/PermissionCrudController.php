<?php

namespace KDA\Backpack\Auth\Http\Controllers\Admin;

use Backpack\PermissionManager\app\Http\Controllers as BackpackControllers;
use Backpack\CRUD\app\Library\Widget;

class PermissionCrudController extends BackpackControllers\PermissionCrudController
{

    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        parent::setup();
        $this->loadPermissions('permission');
    }

    public function setupListOperation()
    {
        parent::setupListOperation();
        if (config('kda.backpack.auth.show_warning')) {
            kda_no_warranty_alert();
        }
    }
}
