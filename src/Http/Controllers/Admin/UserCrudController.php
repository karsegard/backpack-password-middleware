<?php

namespace KDA\Backpack\Auth\Http\Controllers\Admin;

use Backpack\PermissionManager\app\Http\Controllers as BackpackControllers;

class UserCrudController extends BackpackControllers\UserCrudController{

    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;

    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
  /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        parent::setup();
        $this->loadPermissions('user');

    }
  /**
     * Handle password input fields.
     */
    protected function handleUserShouldChangePassword($request)
    {
        // Remove fields not present on the user.
        $request->request->set('force_password_change',1);
       

        return $request;
    }

    public function setupListOperation(){
        parent::setupListOperation();
        $this->crud->addColumn([
            'type'=>'closure',
            'label'=> 'Actif',
            'function' => function ($entry) {
                return $entry->enabled ? 'oui':'non';
            },
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    return $entry->enabled ?  'badge badge-success' :  'badge badge-error';
                },
            ],
        ]);
        $this->crud->addColumn([
            'type'=>'closure',
            'label'=> 'Activé',
            'function' => function ($entry) {
                if(!$entry->force_password_change){
                    return 'oui';
                }
                return $entry->password_changed_on !== null ? 'oui':'non';
            },
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if(!$entry->force_password_change){
                        return 'badge badge-primary';
                    }
                    return ( $entry->password_changed_on !== null) ?  'badge badge-success' :  'badge badge-error';
                },
            ],
        ]);
    }

    public function setupCreateOperation()
    {
        $this->crud->addField([
            'name'=>'force_password_change',
            'type'=>'hidden', 
            'value'=>'1'
        ]);

       
        parent::setupCreateOperation();
        $this->crud->addField([
            'name'=>'enabled',
            'type'=>'toggle', 
            'label'=>'Activé',
            'default'=>'1',
            'view_namespace'=>'kda-backpack-custom-fields::fields', 
        ]);

    }

    public function setupUpdateOperation()
    {
       
       
        parent::setupUpdateOperation();
       
        $this->crud->field(['roles', 'permissions'])->tab('permissions');
        $this->crud->field('password')->tab('password');
        $this->crud->field('password_confirmation')->tab('password');
        if(config('kda.backpack.auth.allow_force_password_edit')){
            $this->crud->addField([
                'name'=>'force_password_change',
                'label'=>'Forcer le changement de mot de passe',
                'type'=>'toggle', 
                'hint'=> 'force l\'utilisateur à changer son mot de passe à la prochaine connexion',
                'view_namespace'=>'kda-backpack-custom-fields::fields', 
            ]);
        }
        $this->crud->addField([
            'name'=>'enabled',
            'label'=> 'Actif',
            'hint'=> 'désactiver l\'utilisateur l\'empêche de se connecter',
            'type'=>'toggle', 
            'view_namespace'=>'kda-backpack-custom-fields::fields', 
        ]);

    }


    /**
     * Store a newly created resource in the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->crud->setRequest($this->crud->validateRequest());
        $this->crud->setRequest($this->handlePasswordInput($this->crud->getRequest()));
        $this->crud->setRequest($this->handleUserShouldChangePassword($this->crud->getRequest()));
        $this->crud->unsetValidation(); // validation has already been run
      //  dump($this->crud->getRequest()->input('force_password_change'));
      //  dd($this->crud->getRequest()->input('password'));

        return $this->traitStore();
    }
}