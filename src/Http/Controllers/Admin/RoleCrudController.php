<?php

namespace KDA\Backpack\Auth\Http\Controllers\Admin;

use Backpack\PermissionManager\app\Http\Controllers as BackpackControllers;
use Backpack\CRUD\app\Library\Widget;

class RoleCrudController extends BackpackControllers\RoleCrudController
{

    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        parent::setup();
        $this->loadPermissions('role');
    }


    public function setupListOperation()
    {
        parent::setupListOperation();
        if (config('kda.backpack.auth.show_warning')) {
            Widget::add([
                'type'         => 'alert',
                'class'        => 'alert alert-error mb-2',
                'heading'      => 'Important !',
                'content'      => 'Modifier ces paramêtres peut empêcher le site de fonctionner.',
                'close_button' => false, // show close button or not
            ]);
        }
    }
}
