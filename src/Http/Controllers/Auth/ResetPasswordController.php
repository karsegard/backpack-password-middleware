<?php  


namespace KDA\Backpack\Auth\Http\Controllers\Auth;


use Backpack\CRUD\app\Http\Controllers\Auth\ResetPasswordController as BackpackResetPasswordController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
class ResetPasswordController extends BackpackResetPasswordController{
    

    use \KDA\Backpack\Auth\Http\Controllers\Traits\ResetsPasswords;
   
     /**
     * Get the guard to be used during logout.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return backpack_auth();
    }
  /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm(Request $request)
    {
        $this->data['title'] = trans('backpack::base.reset_password'); // set the page title
        $this->data['email'] = $request->query('e');
        $this->data['forced'] = session('weak_password')===true;


        return view('kda/backpack/auth::auth.passwords.email', $this->data);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        $passwords = config('backpack.base.passwords', config('auth.defaults.passwords'));

        return Password::broker($passwords);
    }
}