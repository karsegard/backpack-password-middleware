<?php

namespace KDA\Backpack\Auth\Http\Controllers\Traits;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Contracts\Auth\Authenticatable;
trait ResetsPasswords
{

    use \Backpack\CRUD\app\Library\Auth\ResetsPasswords {resetPassword as defaultResetPassword;}

   
    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $this->setUserPassword($user, $password);

        $user->setRememberToken(Str::random(60));
        $user->password_changed_on = new \DateTime();
        $user->save();
        session(['weak_password' => false]);

        event(new PasswordReset($user));
        $this->guard()->login( $user);
    }


    /**
     * Get the path the user should be redirected to after password reset.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function redirectTo()
    {
        return backpack_url();
    }

}
