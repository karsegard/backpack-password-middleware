<?php

namespace KDA\Backpack\Auth\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class ForcePasswordChange
{

    protected $except = [
        '/admin/password/reset', // allow this URL to load even if "weak_password" exists
                                // add more for your change-password CRUD URLs
    ];

    protected function inExceptArray($request)
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->fullUrlIs($except) || $request->is($except)) {
                return true;
            }
        }

        return false;
    }

    public function handle($request, Closure $next)
    {
       if(session("weak_password")===true ){
            if (!$this->inExceptArray($request) ) {
                $e = backpack_user()->email;
                backpack_auth()->logout();
                return redirect("/admin/password/reset?e=".$e);
            }
        }
        if(backpack_user()->enabled === false){
            backpack_auth()->logout();
            return redirect("/admin/login");
        }

        return $next($request);
    }
}