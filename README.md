# backpack-auth

## about

It replace some auth part in backpack allowing users to be forced to change password on the first login.
It also provide dev tools such as seeders & seeds for @fdt2k/backpack-devtools

## Installation

    sail composer require fdt2k/backpack-auth --prefer-source

# admin permissions & roles

## new way

    sail artisan kda:backpack-auth:install

## previous way
    sail artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="migrations"
    sail artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="config"
    sail artisan vendor:publish --provider="Backpack\PermissionManager\PermissionManagerServiceProvider"
    sail artisan vendor:publish --provider="KDA\Backpack\Auth\ServiceProvider" --tag="migrations"
    sail artisan vendor:publish --provider="KDA\Backpack\Auth\ServiceProvider" --tag="config"

then **remove routes/custom/permissions.php**


# Configure Administrator privileges

#### admin has all permissions

change app/Providers/AuthServiceProvider.php

    public function boot()
    {
        //....
        Gate::before(function ($user, $ability) {
            return $user->hasRole('admin') ? true : null;
        });

        ///   ....
    }

### in config/backpack/base.php

disable setup_auth_routes

add ForcePasswordChange middleware and uncomment UseBackpackAuthGuardInsteadOfDefaultAuthGuard middleware

     'middleware_class' => [
         // others
        \KDA\Backpack\Auth\Http\Middleware\ForcePasswordChange::class,
        \Backpack\CRUD\app\Http\Middleware\UseBackpackAuthGuardInsteadOfDefaultAuthGuard::class
       //others
    ],

to **config/backpack/base.php**

### optionally publish needed file to your project

    sail artisan vendor:publish --provider="KDA\Backpack\Auth\ServiceProvider" --tag="models"
    sail artisan vendor:publish --provider="KDA\Backpack\Auth\ServiceProvider" --tag="migrations"
    sail artisan vendor:publish --provider="KDA\Backpack\Auth\ServiceProvider" --tag="config"
    sail artisan vendor:publish --provider="KDA\Backpack\Auth\ServiceProvider" --tag="routes"

### configure seeder


#### publish if needed

    sail artisan vendor:publish --provider="KDA\Backpack\Auth\ServiceProvider" --tag="seeders"

#### if using @fdt2k/backpack-devtools

you can register theses seeds to config/kda/devtools.php

    'seeds' => [
        'users',
        'roles',
        'permissions',
        'role_has_permissions',
        'model_has_roles',
        'model_has_permissions'
    ]

#### automate db:seed

in database/seeders/DatabaseSeeder.php

    public function run()
    {
        $this->call([
            \KDA\Backpack\Auth\Seeders\AdminUserSeeder::class, // if needed & using config generation
            \KDA\Backpack\Auth\Seeders\Seeder::class,
        ]);
    }

in config/kda/backpack/auth.php you'll find an example of pregenerated user, roles & permissions. Publish it with 

    sail artisan vendor:publish --provider="KDA\Backpack\Auth\ServiceProvider" --tag="config"    

then you can run the role & permissions & user generator with

    sail artisan db:seed --class="\KDA\Backpack\Auth\Seeders\AdminUserSeeder"

### configure admin controllers (optional)

    sail artisan vendor:publish --provider="KDA\Backpack\Auth\ServiceProvider" --tag="controllers"

#### update routes/backpack/permissionmanager.php (optional)

replace namespace "Backpack\PermissionManager\app\Http\Controllers" with "App\Http\Controllers\Admin"

## \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission

This is an helper to automagically bind permission to CRUD Operations for a Backpack controller

     public function setup()
    {
        parent::setup();
        $this->loadPermissions('group');

    }

This will bind group.read && group.write permission like this by default

        //permissions
        'read' => [
            //operations
            'list',
            'show',
            'preview'
        ],
        'write'=> [
            'create',
            'delete',
            'update',
            'edit'
        ]

You can specify other permissions like this

The second parameter define to which operation you want to setup permissions

The third is which permissions is applied to which operations

     public function setup()
    {
        parent::setup();
        //only apply permission on the list operation.
        $this->loadPermissions('anothergroup',['list'],['read'=>['show']]);

    }
